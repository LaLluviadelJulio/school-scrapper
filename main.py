"""Launching the main script and graphical shell."""
import sys

from PyQt5.QtGui import QIcon, QPainter, QPixmap
from PyQt5.QtWidgets import QApplication, QPushButton, QWidget

from school_scrapper import get_home_task


class MainWindow(QWidget):
    """The main application window."""

    def __init__(self):
        """Initialize of the Main application window."""
        super().__init__()
        self.initUI()

    def initUI(self):
        """Define the characteristics of the Main application window."""
        self.setGeometry(300, 300, 300, 220)
        self.setWindowTitle('Моя домашка')
        self.setWindowIcon(QIcon('img/monkey.png'))

        qbtn = QPushButton('Получить домашку!', self)
        qbtn.clicked.connect(get_home_task)
        qbtn.resize(150, 100)
        qbtn.move(75, 100)

        qbtn.setStyleSheet('''
            background-color: red;
            border-style: outset;
            border-width: 2px;
            border-radius: 10px;
            border-color: beige;
            font: bold 14px;
            color: white;
        ''') # noqa Q001
        self.show()

    def paintEvent(self, a0):
        """
        Fill the Main application window with a background image.

        Args:
            a0 (QtGui.QPaintEvent): object of filling.

        Returns:
            None:
        """
        painter = QPainter(self)
        pixmap = QPixmap('img/dvoyki.jpg')
        painter.drawPixmap(self.rect(), pixmap)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec())
