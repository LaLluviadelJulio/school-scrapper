<!-- PROJECT SHIELDS -->
[![MIT License][license-shield]][license-url]
<p><a name="readme-top"></a></p>


<!-- PROJECT LOGO -->
<br />
<div align="center">
  <h3 align="center">School site scrapper</h3>

  <p align="center">
    Home-task scrapper(parser) from the school website.
  </p>
</div>


<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
      <ul>
        <li><a href="#launch">Launch</a></li>
        <li><a href="#problems">School site problems</a></li>
        <li><a href="#result">Result</a></li>
      </ul>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
<p><a name="about-the-project"></a></p>

The parser that allows you to quickly and conveniently get all the information about home-task. 
The script has a simple graphical shell.

Authorization is performed on the site, going to pages with data on home-task for the current and next weeks, 
obtaining and aggregating the necessary data.

Next, the generated tables are written to the .xlsx file so that the tasks of each day are recorded on a separate sheet.

In addition, the home-task for tomorrow will be sent to the user in a Telegram using a bot.

Implemented logging of the scrapping progress.

_Парсер, который позволяет быстро и удобно получить всю информацию о домашних заданиях.Скрипт имеет простейшую графическую оболочку._

_Выполняется авторизация на сайте, переход на страницы с данными о домашних заданиях на текущую и следующую недели, получение и агрегация необходимых данных._

_Далее выполняется запись сформированных таблиц в файл .xlsx так, что задания каждого дня записываются на отдельный лист._

_Кроме того, домашнее задание на завтрашний день будет отослано ученику в Телеграм при помощи бота._

_Реализовано логирование хода выполнения скраппинга._
<p align="right">(<a href="#readme-top">back to top</a>)</p>



### Built With
<p><a name="built-with"></a></p>

[![Python][Python logo]][Python url]
[![PyQT][PyQT5 logo]][PyQT5 url]
[![Selenium][Selenium logo]][Selenium url]
[![Flake8][Flake8 logo]][Flake8 url]


<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- USAGE EXAMPLES -->
## Usage
<p><a name="usage"></a></p>

### Launch
<p><a name="launch"></a></p>

Use **main.py** to run using a graphical shell.

_Используйте **main.py** для запуска с использованием графической оболочки._

You'll see a launcher window with a single button starting the parser script.

_При запуске приложения появится окно лаунчера с единственной кнопкой, запускающей скрипт парсера._

[![School scrapper main Screen Shot](https://gitlab.com/LaLluviadelJulio/school-scrapper/-/raw/main/public/img_docs/main.png)]()

Next, the script will start working: opening a browser window controlled by Selenium,
logging in to the school website, going to the diary pages and getting home-task.

_Далее начнется работа скрипта: открытие окна браузера, управляемого Selenium,
авторизация на школьном сайте, переход на страницы дневника и получение домашнего задания._

### School site problems
<p><a name="problems"></a></p>

If problems arise during the authorization process on the school website 
(primarily related to the dynamic loading of the authorization form and 
the expiration of the script timeout), a message box will appear about the 
need to re-run the script by clicking the button. 
The browser window will be closed at the same time.

_Если в процессе авторизации на школьном сайте возникнут проблемы 
(связанные, прежде всего, с динамической загрузкой формы для авторизации и 
истечением времени ожидания скрипта), появится сообщение о необходимости 
повторно запустить скрипт, нажав кнопку. Окно браузера будет при этом закрыто._

[![Message box Screen Shot](https://gitlab.com/LaLluviadelJulio/school-scrapper/-/raw/main/public/img_docs/msg_box.png?ref_type=heads)]()


### Result
<p><a name="result"></a></p>

The result of the script is to obtain ordered information about home-task:

* in the form of .xlsx file, where home-task assignments for the current and next weeks are arranged
as a separate sheet for each day

_Итогом работы скрипта является получение упорядоченной информации о домашних заданиях:_
* _в виде .xlsx файла, где домашние задания на текущую и следующую недели оформлены
в виде отдельного листа для каждого дня_

[![.xlsx Screen Shot](https://gitlab.com/LaLluviadelJulio/school-scrapper/-/raw/main/public/img_docs/xlsx.png?ref_type=heads)]()

* in the form of a Telegram message about home-task for tomorrow. If tomorrow falls on a weekend, home-task assignments for the following Monday will be sent.


* _в виде сообщения в Telegram о домашнем задании на завтрашний день.
Если завтрашний день приходится на выходные, будут высланы домашние задания на следующий понедельник._

[![Telegram message Screen Shot](https://gitlab.com/LaLluviadelJulio/school-scrapper/-/raw/main/public/img_docs/tlg_msg.png)]()


<!-- LICENSE -->
## License
<p><a name="license"></a></p>

Distributed under the MIT License.

Используется лицензия MIT.

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- CONTACT -->
## Contact
<p><a name="contact"></a></p>

Marina Marmalyukova - inspiracion@yandex.ru

Project Link: [https://gitlab.com/LaLluviadelJulio/school-scrapper](https://gitlab.com/LaLluviadelJulio/school-scrapper)

<p align="right">(<a href="#readme-top">back to top</a>)</p>


<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[Python logo]: https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white
[Python url]: https://www.python.org/?hl=RU
[PyQT5 logo]: https://img.shields.io/badge/PyQT5-218440?style=for-the-badge&logoColor=white
[PyQT5 url]: https://pypi.org/project/PyQt5/
[Selenium logo]: https://img.shields.io/badge/Selenium-634890?style=for-the-badge&logo=selenium&logoColor=white
[Selenium url]: https://www.selenium.dev/
[Flake8 logo]: https://img.shields.io/badge/Flake8-07405E?style=for-the-badge&logo=flake8&logo=sqlite&logoColor=white
[Flake8 url]: https://flake8.pycqa.org/