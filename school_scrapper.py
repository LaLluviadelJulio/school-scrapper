"""The main script of the scrapper of the school website."""

import logging
import os
import time
from datetime import date, timedelta

from PyQt5.QtWidgets import QMessageBox

from dotenv import load_dotenv

from notifiers import get_notifier

import pandas as pd

from selenium import webdriver
from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import (
    presence_of_element_located
)
from selenium.webdriver.support.ui import WebDriverWait

from third_part.append_to_excel import append_df_to_excel

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)


class HomeTaskScrapper:
    """The scrapper object of the school website."""

    # pylint: disable=too-many-instance-attributes

    def __init__(self):
        """Initialize of the scrapper object."""
        self.driver = webdriver.Firefox()
        self.hw_list = []
        self.login = os.environ.get('LOGIN')
        self.password = os.environ.get('PASSWORD')
        self.auth_url = os.environ.get('AUTH_URL')
        self.first_week_url = os.environ.get('FIRST_WEEK_URL')
        self.second_week_url = os.environ.get('SECOND_WEEK_URL')
        self.file_name = os.environ.get('FILE_NAME')
        self.token = os.environ.get('TLG_TOKEN')
        self.tlg_chat_id = os.environ.get('TLG_CHAT_ID')
        self.logger = logging.getLogger(__name__)
        self.config_logger()

    def config_logger(self):
        """Configure logger."""
        self.logger.setLevel(logging.INFO)
        log_handler = logging.FileHandler(f'{__name__}.log', mode='w')
        log_formatter = logging.Formatter(
            '%(name)s %(asctime)s %(levelname)s %(message)s')
        log_handler.setFormatter(log_formatter)
        self.logger.addHandler(log_handler)

    def process_home_task(self):
        """Get home-task and make it available for viewing."""
        self.logger.info('Scrapping started.')
        try:
            self.authorize()
            self.logger.info('Get scrapping data.')
            self.get_week_result(self.first_week_url)
            self.get_week_result(self.second_week_url)
            self.hw_to_xlsx()
            self.driver.close()
            self.logger.info('Scrapping finished.')

        except Exception as err:
            self.logger.error('Failed to get home-task due %s', err)

    def get_url(self, url, wait_condition):
        """
        Get url and wait until condition is fulfilled.

        Args:
            url (str): url to get.
            wait_condition (tuple): condition that must be fulfilled.

        Returns:
            None:
        """
        self.driver.get(url)
        WebDriverWait(driver=self.driver, timeout=25).until(
            presence_of_element_located(wait_condition)
        )

    def authorize(self):
        """Authorize on the school site."""
        self.logger.info('Authorize on school site.')
        try:
            self.get_url(self.auth_url, (By.XPATH, '//button[@type="submit"]'))
            self.driver.find_element(
                By.XPATH, '//input[1]').send_keys(self.login)
            self.driver.find_element(
                By.XPATH, '//input[@type="password"]').send_keys(
                self.password)
            time.sleep(2)
            self.driver.find_element(
                By.XPATH, '//button[@type="submit"]').click()
        except NoSuchElementException as err:
            self.logger.error('Error of auth: %s', err)
            self.driver.close()
            self.create_msg_box()

    def get_week_result(self, url):
        """
        Find and scrap week home-task data.

        Args:
            url (str): url of the week home-task.

        Returns:
            None:
        """
        self.driver.get(url)
        WebDriverWait(driver=self.driver, timeout=15).until(
            presence_of_element_located((By.CLASS_NAME, 'dnevnik-day'))
        )
        hw_res_cur_week = self.driver.find_elements(
            By.CLASS_NAME, 'dnevnik-day')
        self.scrap_hw(hw_res_cur_week)

    def scrap_hw(self, result):
        """
        Scrap home-task data.

        Args:
            result (list[selenium.webdriver.remote.webelement.WebElement]):
                list of scrapped objects.
        Returns:
            None:
        """
        for item in result:
            day_name = item.find_element(By.CLASS_NAME, 'dnevnik-day__title')
            day_name = day_name.text.strip('\n ')
            lessons = item.find_elements(By.CLASS_NAME, 'dnevnik-lesson')
            if lessons:
                hw_day_data = {}

                for lesson in lessons:
                    try:
                        subject = lesson.find_element(
                            By.CLASS_NAME, 'js-rt_licey-dnevnik-subject').text
                        try:
                            task = lesson.find_element(
                                By.CLASS_NAME, 'dnevnik-lesson__task')
                            task = task.text.strip('\n ').split('\n')[0]
                        except NoSuchElementException:
                            task = '-'
                        hw_day_data[subject] = task
                    except NoSuchElementException:
                        pass
                self.hw_list.append({day_name: hw_day_data})

    def hw_to_xlsx(self, is_first_sheet=False):
        """
        Create xlsx file with home-task for each day.

        Args:
            is_first_sheet(bool): flag that determines whether
                                  the sheet is the first

        Returns:
            None:
        """
        self.logger.info('Process scrapped data to xlsx.')
        today = date.today()
        if today.weekday() == 4:
            days_num = 3
        elif today.weekday() == 5:
            days_num = 2
        else:
            days_num = 1
        tomorrow_str = (today + timedelta(days=days_num)).strftime('%d.%m')
        for day_data in self.hw_list:
            df_result = pd.DataFrame(day_data)
            date_str = next(iter(day_data))
            if date_str.split(sep=', ')[1] == tomorrow_str:
                self.send_tlg_notification(day_data.get(date_str))

            if not is_first_sheet:
                with pd.ExcelWriter(
                        f'{self.file_name}.xlsx', engine='xlsxwriter') as e_w:
                    df_result.to_excel(e_w, sheet_name=list(day_data)[0])
                is_first_sheet = True
            else:
                append_df_to_excel(
                    f'{self.file_name}.xlsx', df_result,
                    sheet_name=list(day_data)[0]
                )

    def send_tlg_notification(self, day_data):
        """
        Send telegram notification about tomorrow home-task.

        Args:
            day_data (dict): tomorrow home-task.

        Returns:
            None:
        """
        self.logger.info('Send tomorrow home-task on telegram.')
        try:
            telegram = get_notifier('telegram')
            message_text = '\n'.join(
                f'{subj}: {task}' for subj, task in day_data.items()
            )
            telegram.notify(
                token=self.token, chat_id=self.tlg_chat_id,
                message=message_text
            )
        except Exception as err:
            self.logger.error(
                'Failed to send tomorrow home-task on telegram due %s', err)

    @staticmethod
    def create_msg_box():
        """Create message box about the need to re-request data."""
        msg_box = QMessageBox()
        msg_box.setIcon(QMessageBox.Critical)
        msg_box.setText(
            'Загрузка школьного сайта была слишком долгой '
            'и домашка не была получена. Нажми кнопку получения домашки '
            'еще раз!')
        msg_box.setWindowTitle('Внимание! Домашка не была получена')
        msg_box.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        msg_box.exec_()


def get_home_task():
    """
    Start getting home-task process.

    Returns:
        None:
    """
    home_task_obj = HomeTaskScrapper()
    home_task_obj.process_home_task()


if __name__ == '__main__':
    get_home_task()
